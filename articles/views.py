import logging
from collections import namedtuple

from django.contrib.auth.models import Group
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, permissions, mixins, generics, filters
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db.models import Q
from articles import serializers
from articles.models import User, Article

logger = logging.getLogger(__name__)


STATUS = namedtuple("Status", ("ERROR", "SUCCESS"))("error", "success")
ROLES = namedtuple("Roles", ("WRITER", "EDITOR"))("writer", "editor")

GROUPS = {
    ROLES.WRITER: "writers",
    ROLES.EDITOR: "editors"
}


def is_editor(user):
    return user in Group.objects.get(name=GROUPS.get(ROLES.EDITOR)).user_set.all()


def is_writer(user):
    return user in Group.objects.get(name=GROUPS.get(ROLES.WRITER)).user_set.all()


class SignUpView(APIView):
    permission_classes = (permissions.AllowAny, )

    def post(self, request):
        errors = {"critical": "No data provided"}
        data = request.data
        if data:
            serializer = serializers.RegistrationSerializer(data=data)
            if serializer.is_valid():
                role = serializer.validated_data["role"] or ROLES.WRITER

                user = User.objects.create(email=serializer.validated_data["email"])
                user.set_password(serializer.validated_data["password"])
                user.groups.add(Group.objects.get(name=GROUPS.get(role)))
                user.save()

                return Response({
                    "email": user.email,
                    "token": user.auth_token
                }, status=status.HTTP_201_CREATED)
            else:
                errors = serializer.errors
        return Response(errors, status=status.HTTP_400_BAD_REQUEST)


class SignInView(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        errors = {"critical": "No data provided"}
        data = request.data
        if data:
            serializer = serializers.AuthUserSerializer(data=data)
            if serializer.is_valid():
                user = User.objects.get(email=serializer.validated_data["email"])
                if user.check_password(serializer.validated_data["password"]):
                    return Response({
                        "email": user.email,
                        "token": user.auth_token.key
                    })
            else:
                errors = serializer.errors
        return Response(errors, status=status.HTTP_400_BAD_REQUEST)


class ProfileView(generics.RetrieveAPIView):
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = serializers.ProfileSerializer
    queryset = User.objects.all()


class ArticleView(mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):
    permission_classes = (permissions.DjangoModelPermissionsOrAnonReadOnly, )

    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    search_fields = ('title', 'text', 'author__first_name', 'author__last_name')
    filter_fields = ('author',)

    def get_serializer_class(self):
        if self.request.user.is_authenticated:
            return serializers.UserArticleSerializer
        return serializers.AnonymousArticleSerializer

    def get_queryset(self):
        queryset = Article.objects.all()
        approved = Q(approved=True)

        if is_editor(self.request.user):
            return queryset

        if is_writer(self.request.user):
            return queryset.filter(approved | Q(author=self.request.user))

        return queryset.filter(approved)

    def get(self, request, *args, **kwargs):
        if kwargs.get('pk'):
            return self.retrieve(request, *args, **kwargs)
        return self.list(request, *args, **kwargs)

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        serializer.instance.author = request.user
        serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class ArticleApproveView(generics.GenericAPIView):
    permission_classes = (permissions.DjangoModelPermissions, )
    queryset = Article.objects.all()
    serializer_class = serializers.UserArticleSerializer

    def put(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data={'approved': True}, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)
