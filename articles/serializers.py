import logging

from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from articles.models import User, Article

logger = logging.getLogger(__name__)


class AuthorRelatedField(serializers.StringRelatedField):
    def to_representation(self, value):
        return f'{value.first_name} {value.last_name}'


class AuthUserSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField(max_length=64, min_length=8)


class RegistrationSerializer(AuthUserSerializer):
    email = serializers.EmailField(validators=[UniqueValidator(queryset=User.objects.all())])
    role = serializers.CharField(max_length=32)
    first_name = serializers.CharField(max_length=256, required=False)
    last_name = serializers.CharField(max_length=256, required=False)


class ProfileSerializer(serializers.ModelSerializer):
    groups = serializers.StringRelatedField(many=True)

    class Meta:
        model = User
        exclude = ('is_staff', 'is_superuser', 'password', 'user_permissions')


class AnonymousArticleSerializer(serializers.ModelSerializer):
    author = AuthorRelatedField()
    author_id = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Article
        exclude = ['approved',]


class UserArticleSerializer(serializers.ModelSerializer):
    author = AuthorRelatedField()
    author_id = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Article
        fields = '__all__'
