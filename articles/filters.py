from django_filters import rest_framework as filters
from django.db.models import Q


class ArticleSearchFilter(filters.FilterSet):
    search = filters.CharFilter(method='search_filter')

    def search_filter(self, queryset, name, value):
        return queryset.filter(Q(author__first_name__icontains=value) | Q(author__last_name__icontains=value))