# Test task from Archer Software

## Endpoints

### User endpoints

1. /register (POST)

    - Headers:
        - Content-Type: 'application/json'
    
    - Request data example
        - ```
            {"email": "<email@address>", "password": "<password>"}
        ```
    - Response example:
        - Success
            - HTTP code 201 (created)
            - Data:
                ```
                {"email": "<email@address>", "token": "<auth_token>"}
                ```
        - Error
            - HTTP code 400 (bad request)
            - Data: JSON with errors description

2. /login (POST)

    - Headers:
        - Content-Type: 'application/json'
    
    - Request data example:
        - ```
            {"email": "<email@address>", "password": "<password>"}
        ```
    - Response example
        - Success
            - HTTP code 201 (created)
            - Data:
                ```
                {"email": "<email@address>", "token": "<auth_token>"}
                ```
        - Error
            - HTTP code 400 (bad request)
            - Data: JSON with errors description

3. /profile/:pk (GET)
    - Headers:
        - Content-Type: 'application/json'
        - Authorization: 'Token <auth_token>'
    - Response example:
        - HTTP code 200 (ok)
        - Data:
            ```
            {
                "id": 1,
                "groups": [
                    "editors"
                ],
                "last_login": "2018-06-17T10:12:23.468416Z",
                "email": "<user@email>",
                "first_name": "<user_first_name>",
                "last_name": "<user_last_name>",
                "registered": "<registration_date>",
                "is_active": true
            }
            ```
### Articles endpoints
1. /articles (GET)
    - Headers:
        - Content-Type: 'application/json'
        - Authorization: 'Token <auth_token>' (Optional)
    - Optional GET parameters:
        - search: <search query>
        - author: <author_id>
    - Response example:
        - HTTP code 200 (ok)
        - Data:
        ```
          {
            "count": 1,
            "next": null,
            "previous": null,
            "results": [
              {
                "id": 1,
                "author": "<author full name>",
                "author_id": 1,
                "title": "<article_title>",
                "text": "<article_text>",
                "approved": true
              }
            ]
          }
        ```

2. /articles (POST)
    - Headers:
        - Content-Type: 'application/json'
        - Authorization: 'Token <auth_token>'
    - Data example:
        ```
          {
            "title": "<article_title>",
            "text": "<article_text>"
          }
        ```
    - Response example:
        - HTTP code 201 (created)
        - Data:
        ```
          {
            "id": 1,
            "author": "<author_name>",
            "author_id": 1,
            "title": "<article_title>",
            "text": "<article_text>",
            "approved": false
          }
        ```

3. /articles/:pk (GET)
    - Headers:
        - Content-Type: 'application/json'
        - Authorization: 'Token <auth_token>' (optional)
    - Response example:
        - HTTP code 200 (ok)
        - Data:
        ```
          {
            "id": 1,
            "author": "<author_name>",
            "author_id": 1,
            "title": "<article_title>",
            "text": "<article_text>",
            "approved": true
          }
        ```

4. /articles/:pk/approve (PUT)
    - Headers:
        - Content-Type: 'application/json'
        - Authorization: 'Token <auth_token>'
    - Data:
        ```
          {}
        ```
    - Response example:
        - HTTP code 200 (ok)
        - Data:
            ```
              {
                "id": 1,
                "author": "<author_name>",
                "author_id": 1,
                "title": "<article_title>",
                "text": "<article_text>",
                "approved": true
              }
            ```        
